import { Document } from 'mongoose';

export default interface IShoppingList extends Document {
    name: string;
    userId: string;
    deletedAt: string;
}
