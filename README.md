# Shopping List authentificate App with MongoDB in Docker Containers



## Installation

Clone the repo:

```
git clone https://lazar96f@bitbucket.org/lazar96f/node-mongo-shopping-list.git

```

## How to start project:
```
docker-compose up -d --build
```
```
docker-compose up
```

## Description

**Singup user:**
 http://localhost:3000/api/user/register
```json
 body {
     "name": "Test",
     "email": "test@test.com",
     "password": "12345678"
 }
```
 **User login**
  http://localhost:3000/api/user/login

```json
  body{
      "email": "test@test.com",
      "password": "12345"
  }
```
If you are successfully logged in you will receive a access-token string.
For all other routes you need set access-token string value to header key named: auth-token, cause that is how authentication work.

**Creates shopping list**
http://localhost:3000/api/shopping/create
```json
body{
    "name": "Test",
    "productList": [
        {
            "name": "test",
            "quantity": 1
        },
    ]
}
```

**Change List name**
http://localhost:3000/api/shopping/update/name/:id

Param id - shopping list id for update


**Delete shopping list**
http://localhost:3000/api/shopping/delete/:id

param id - shopping list id for delete
Soft delete



**Creating reports**
http://localhost:3000/api/product/report

For the time-interval send, it returns the array of products and the total quantity of each product.

```json
body {
    "startInterval": "2018-08-01T11:50:19.467Z",
    "endInterval": "2022-07-01T11:50:19.467Z"
}
```

**Reset password**
 http://localhost:3000/api/user/password/reset

```json
    body{
        "email": "test@test.com",
        "password": "New password"
    }
```

All routes except singup and login are protected with auth middleware.