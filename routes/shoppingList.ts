import { Router } from 'express'
import { verifyToken as authMiddleware } from '../helpers/services'
import { createShoppingList, getAll, updateName, deleteList } from '../controller/shoppingList'

const router = Router();

router.post('/create', authMiddleware, createShoppingList);
router.get('/lists', authMiddleware, getAll)
router.put('/update/name/:id', authMiddleware, updateName)
router.put('/delete/:id', authMiddleware, deleteList)

export default router;