import * as express from 'express'
import { verifyToken as authMiddleware } from '../helpers/services'
import { signUpUser, loginUser } from '../controller/auth'
import {resetPassword} from '../controller/user'
import { getUsers } from '../controller/user'
const router = express.Router();

router.post('/register', signUpUser)
router.post('/login', loginUser)
router.get('/users', authMiddleware, getUsers)
router.put('/password/reset',authMiddleware, resetPassword)

export default router;