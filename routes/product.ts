import * as express from 'express'
import { verifyToken as authMiddleware } from '../helpers/services'
import { createProduct, getAll, createReportForTimeInterval } from '../controller/product'
const router = express.Router();

router.post('/create', authMiddleware, createProduct)
router.get('/list', authMiddleware, getAll)
router.post('/report', authMiddleware, createReportForTimeInterval)

export default router;