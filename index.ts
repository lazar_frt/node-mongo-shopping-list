import express, { urlencoded, json } from 'express';
import authRoute from './routes/auth';
import productRoute from './routes/product'
import shoppingRoute from './routes/shoppingList'
import dotenv from 'dotenv'
import { dbConnection } from './helpers/services'
import morgan from 'morgan'

dotenv.config();
const app = express();
app.use(urlencoded({ extended: false }));
app.use(json());

// Connect to MongoDB
dbConnection();


// Logger
app.use(morgan('dev'));


//Routes
app.use('/api/user', authRoute);
app.use('/api/product', productRoute);
app.use('/api/shopping', shoppingRoute);


const port = process.env.SERVER_PORT || 5000;
app.listen(port, () => console.log(`Server running on port ${port} :)`));
;
