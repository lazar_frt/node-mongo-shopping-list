import { Request, Response } from 'express'
import User from '../models/user';
import { genSalt, hash } from 'bcryptjs';


export const getUsers = async (req: Request, res: Response):Promise<any> => {
    try {
        const users = await User.find();
        if (!users) return res.send('Shopping lists not found')
        res.json(users);
    } catch (err) {
        res.status(400).send(err)
    }
}

export const resetPassword = async (req: Request, res: Response): Promise<any> => {
    const user = await User.findOne({ email: req.body.email });
    const salt = await genSalt(10);
    if (!user) return res.status(400).send(`User, ${req.body.email} with provided email not found`);
    const hashedPassword = await hash(req.body.password, salt);
    try {
        await User.findOneAndUpdate({ email: req.body.email }, { password: hashedPassword })
        res.send('Password successfully updated')
    } catch (err) {
        res.status(400).send(err)
    }
}
