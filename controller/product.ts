import { Request, Response } from 'express'
import Product from '../models/product'

type ProductType = {
    name: string,
    quantity: number
}

export const createProduct = async (req: Request, res: Response): Promise<void> => {
    const product = new Product({
        name: req.body.name,
        quantity: req.body.quantity,
        shoppingListId: req.body.shoppingListId
    });

    try {
        await product.save();
        res.json({
            success: Boolean(product),
            message: 'Product created',
            result: product._id
        });
    } catch (err) {
        res.json({
            success: false,
            message: err,
            result: null
        });
    }
}

// Returns all inserted products
export const getAll = async (req: Request, res: Response): Promise<void> => {
    try {
        const products = await Product.find();
        if (!products)  res.send('Products not found')
        res.json({ 
            result: {products}
         });
    } catch (err) {
        res.status(400).json({
            success: false,
            message: err,
            result: null
        })
    }
}

// When the user creates a shopping list we call this method for insert products for that list
export const insertProductsForShoppingList = async (shoppingListId: string, products: [ProductType], res: Response):Promise<void> => {
    try {
        const newProductList = products.map((product: ProductType) => {
            return {
                ...product,
                shoppingListId
            }
        })

        await Product.insertMany(newProductList)

        res.json({
            success: true,
            message: 'Successfully inserted products'
        })
    } catch (err) {
        res.status(400).send(`Faild to insert product, error: ${err}`).json({
            success: false,
            message: 'Faild to insert product',
            result: null
        })
    }
}

//Returns sum of product quantities for provided time-interval
export const createReportForTimeInterval = async (req: Request, res: Response): Promise<void> => {
    const { startInterval, endInterval } = req.body;
    try {
        const products = await Product.aggregate([
            {
                $match: {
                    "createdAt": {
                        $gte: new Date(startInterval),
                        $lte: new Date(endInterval)
                    }
                }
            },
            {
                $group: {
                    _id: "$name",
                    total: { $sum: "$quantity" }
                }
            }
        ])
        if (!products) res.send('Products not found')
        res.json({
            report: {
                products
            }
        });
    } catch (err) {
        res.status(400).send(err)
    }
}