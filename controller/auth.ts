import { Request, Response, NextFunction } from 'express'
import User from '../models/user';
import { genSalt, hash, compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import IUser from 'interfaces/user';
import { validateRegister, validateLogin } from '../helpers/validation'

export const signUpUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { email, name, password } = req.body;
    //Validation
    const validateError = await validateRegister(email, password, name);
    if (validateError) {
        res.send({ Error: validateError.details[0].message });
        return;
    }
    // Check if the user is alredy in the database
    const emailExist = await User.findOne({ email });
    if (emailExist) {
        res.status(400).send('Email alredy exists');
        return;
    }
    // Hash passwords
    const salt = await genSalt(10);
    const hashedPassword = await hash(password, salt);
    // Create new user
    const user: IUser = new User({
        name,
        email,
        password: hashedPassword
    });
    try {
        await user.save();
        res.send({ user: user._id });
    } catch (err) {
        res.status(400).send({ Error: err })
    }
}


export const loginUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { email, password } = req.body;
    //Validation
    const validateError = await validateLogin(email, password);
    if (validateError) {
        res.send({ Error: validateError.details[0].message });
        return;
    }
    // Checking if the email exists
    const user = await User.findOne({ email });
    if (!user) {
        res.status(400).send(`User: ${email} not found`);
        return;
    }
    //Is password correct
    const validPass = await compare(password, user.password);
    if (!validPass) res.status(400).send('Invalid password');


    //Create and assign a token
    const token = sign({ _id: user._id }, process.env.TOKEN_SECRET!);
    res.header('auth-token', token).send({ accessToken: token });
}