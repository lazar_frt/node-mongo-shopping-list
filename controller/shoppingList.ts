import { Request, Response } from 'express'
import ShoppingList from '../models/shoppingList'
import { getLoggedInUser } from '../helpers/services'
import { insertProductsForShoppingList } from '../controller/product'

// Main method for create shopping list and its products
export const createShoppingList = async (req: Request, res: Response): Promise<void> => {
    const { productList, name } = req.body;
    const loggedUser = await getLoggedInUser(req.headers['auth-token']);
    const newShoppingList = new ShoppingList({
        name,
        userId: loggedUser._id
    });

    try {
        await newShoppingList.save();
        await insertProductsForShoppingList(newShoppingList._id, productList, res)
        res.status(201).send({ shoppingListId: newShoppingList._id });
    } catch (err) {
        res.status(400).send(err)
    }
}

export const getAll = async (req: Request, res: Response): Promise<void> => {
    const loggedUser = await getLoggedInUser(req.headers['auth-token']);
    try {
        const lists = await ShoppingList.find({ deletedAt: '', userId: loggedUser._id });
        if (!lists) {
            res.status(400).send('Shopping lists not found')
            return;
        }
        res.status(200).json({ shoppingLists: lists });
    } catch (err) {
        res.status(400).send(err)
    }
}

// Edit name of the shopping list
export const updateName = async (req: Request, res: Response): Promise<void> => {
    const loggedUser = await getLoggedInUser(req.headers['auth-token']);
    try {
        const listToUpdate = await ShoppingList.findOneAndUpdate({ _id: req.params.id, userId: loggedUser._id }, {
            name: req.body.name
        });
        if (!listToUpdate) res.send('Shopping list not found')
        
        res.send('Shopping list successfully updated');
    } catch (err) {
        res.status(400).send(`Faild to update shopping list, error: ${err}`)
    }
}

export const deleteList = async (req: Request, res: Response): Promise<void> => {
    const loggedUser = await getLoggedInUser(req.headers['auth-token']);
    try {
        const listToUpdate = await ShoppingList.findOneAndUpdate({ _id: req.params.id, userId: loggedUser._id }, {
            deletedAt: Date.now().toString()
        });
        if (!listToUpdate) res.json({
            success: false,
            message: 'Shopping list not found'
        });
        res.json({
            success: true,
            message: `Shopping list successfully deleted`
        });
    } catch (err) {
        res.status(400).json({
            success: false,
            message: 'Faild to delete shopping list',
            error: err
        })
    }
}
