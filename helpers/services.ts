import jsonwebtoken from 'jsonwebtoken'
import User from '../models/user';
import { connect } from 'mongoose';
import {Request,Response, NextFunction} from 'express'

export const getLoggedInUser = async (token:any) => {
    const verified: any = jsonwebtoken.verify(token, process.env.TOKEN_SECRET || '');
    // odavde mozemo pokupiti user_id i imamo usera svugdje
    const [user] = await User.find({ _id: verified._id });

    return user;
}


export const dbConnection = () => {
    try{
        connect(
           process.env.DB_CONNECT!,
           { useNewUrlParser: true }
         ).then(() => console.log('Connected to MongoDB :)'))
       }catch(err) {
         console.log ('error:', err.message, err.stack)
         throw new Error(err)
       }
}


export const verifyToken = (req: Request,res: Response,next: NextFunction) => {
    const token = req.header('auth-token');
    if(!token) return res.status(401).send('Authentication is required');
    try{
         jsonwebtoken.verify(token, process.env.TOKEN_SECRET || '');
        next();
    }catch(err) {
        res.status(400).send('Invalid Token');
    }
}
