import Joi from 'joi'




export const validateRegister = async (email:String,password:String,name:String):Promise<any> => {
const schema = Joi.object().keys({
    email: Joi.string().email().lowercase().required(),
    password: Joi.string().min(5).required(),
    name: Joi.string().min(2).required()
})

try{
    await schema.validateAsync({email,password,name});
}catch(err) {
    return err;
}
}


export const validateLogin = async (email:String,password:String) => {
    const schema = Joi.object().keys({
        email: Joi.string().email().lowercase().required(),
        password: Joi.string().min(5).required()
    })
    
    try{
        await schema.validateAsync({email,password});
    }catch(err) {
        return err;
    }
    }
