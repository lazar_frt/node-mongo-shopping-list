import mongoose, { Schema } from 'mongoose';
import IShoppingList from '../interfaces/shoppingList';

const ShoppingListSchema: Schema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    deletedAt: {
        type: String,
        default: ''
    }
},
    {
        timestamps: true
    });

export default mongoose.model<IShoppingList>('ShoppingList', ShoppingListSchema);
