import mongoose, { Schema } from 'mongoose';
import IProduct from 'interfaces/product';

const ProductSchema: Schema = new Schema(
    {
        name: { type: String, required: true, unique: false },
        quantity: { type: Number, required: true },
        shoppingListId: {
            type: Schema.Types.ObjectId,
            ref: "ShopingList",
            required: true
        },
    },
    {
        timestamps: true
    }
);

export default mongoose.model<IProduct>('Product', ProductSchema);
